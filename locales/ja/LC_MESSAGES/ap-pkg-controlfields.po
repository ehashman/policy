# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-08-02 11:56+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

#: ../../ap-pkg-controlfields.rst:2
msgid "Control files and their fields (from old Packaging Manual)"
msgstr ""

#: ../../ap-pkg-controlfields.rst:4
msgid ""
"Many of the tools in the ``dpkg`` suite manipulate data in a common "
"format, known as control files. Binary and source packages have control "
"data as do the ``.changes`` files which control the installation of "
"uploaded files, and ``dpkg``'s internal databases are in a similar "
"format."
msgstr ""

#: ../../ap-pkg-controlfields.rst:13
msgid "Syntax of control files"
msgstr ""

#: ../../ap-pkg-controlfields.rst:15
msgid "See :ref:`s-controlsyntax`."
msgstr ""

#: ../../ap-pkg-controlfields.rst:17
msgid ""
"It is important to note that there are several fields which are optional "
"as far as ``dpkg`` and the related tools are concerned, but which must "
"appear in every Debian package, or whose omission may cause problems."
msgstr ""

#: ../../ap-pkg-controlfields.rst:24
msgid "List of fields"
msgstr ""

#: ../../ap-pkg-controlfields.rst:26
msgid "See :ref:`s-controlfieldslist`."
msgstr ""

#: ../../ap-pkg-controlfields.rst:28
msgid ""
"This section now contains only the fields that didn't belong to the "
"Policy manual."
msgstr ""

#: ../../ap-pkg-controlfields.rst:34
msgid "``Filename`` and ``MSDOS-Filename``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:36
msgid ""
"These fields in ``Packages`` files give the filename(s) of (the parts of)"
" a package in the distribution directories, relative to the root of the "
"Debian hierarchy. If the package has been split into several parts the "
"parts are all listed in order, separated by spaces."
msgstr ""

#: ../../ap-pkg-controlfields.rst:44
msgid "``Size`` and ``MD5sum``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:46
msgid ""
"These fields in ``Packages`` files give the size (in bytes, expressed in "
"decimal) and MD5 checksum of the file(s) which make(s) up a binary "
"package in the distribution. If the package is split into several parts "
"the values for the parts are listed in order, separated by spaces."
msgstr ""

#: ../../ap-pkg-controlfields.rst:54
msgid "``Status``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:56
msgid ""
"This field in ``dpkg``'s status file records whether the user wants a "
"package installed, removed or left alone, whether it is broken (requiring"
" re-installation) or not and what its current state on the system is. "
"Each of these pieces of information is a single word."
msgstr ""

#: ../../ap-pkg-controlfields.rst:64
msgid "``Config-Version``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:66
msgid ""
"If a package is not installed or not configured, this field in ``dpkg``'s"
" status file records the last version of the package which was "
"successfully configured."
msgstr ""

#: ../../ap-pkg-controlfields.rst:73
msgid "``Conffiles``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:75
msgid ""
"This field in ``dpkg``'s status file contains information about the "
"automatically-managed configuration files held by a package. This field "
"should *not* appear anywhere in a package!"
msgstr ""

#: ../../ap-pkg-controlfields.rst:82
msgid "Obsolete fields"
msgstr ""

#: ../../ap-pkg-controlfields.rst:84
msgid ""
"These are still recognized by ``dpkg`` but should not appear anywhere any"
" more."
msgstr ""

#: ../../ap-pkg-controlfields.rst:89
msgid "``Revision``; \\ ``Package-Revision``; \\ ``Package_Revision``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:88
msgid ""
"The Debian revision part of the package version was at one point in a "
"separate control field. This field went through several names."
msgstr ""

#: ../../ap-pkg-controlfields.rst:92
msgid "``Recommended``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:92
msgid "Old name for ``Recommends``."
msgstr ""

#: ../../ap-pkg-controlfields.rst:95
msgid "``Optional``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:95
msgid "Old name for ``Suggests``."
msgstr ""

#: ../../ap-pkg-controlfields.rst:98
msgid "``Class``"
msgstr ""

#: ../../ap-pkg-controlfields.rst:98
msgid "Old name for ``Priority``."
msgstr ""

